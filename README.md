heroiconsgo
===

heroiconsgo is a Go library for accessing [HeroIcons] from within Go applications,
specifically intended for inlining icons within Go web applications.

heroiconsgo currently targets HeroIcons **v2.0.11**.

## Licence

heroiconsgo itself is licenced under the BSD 2-Clause License. The SVG icons themselves
are licenced [MIT][heroicons-licence].

[HeroIcons]: https://heroicons.com/
[heroicons-licence]: https://github.com/tailwindlabs/heroicons/blob/master/LICENSE
