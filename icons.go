/*
 * Copyright (c) 2022, Jamie Mansfield <jmansfield@cadixdev.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

package heroiconsgo

// Get gets the SVG representation of a Feather Icon. An empty string
// will be returned, if no icon exists of the given ID.
func Get(style string, id string) string {
	svg, ok := _icons[style+"-"+id]
	if !ok {
		return ""
	}
	return svg
}
